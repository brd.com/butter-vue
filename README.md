# Getting Started

Butter-vue is a collection of SCSS styles, mixins, and Vue components
for building interfaces based on our standard components.

To install, simply:

```
npm i --save @brd.com/butter-vue
```

And add this line to your `nuxt.config.js` file:

```
modules: [
// ... other modules
'@brd.com/butter-vue/module'
]
```

Then you can integrate global styles into your layout:

```
<style lang="scss">
@import "butter-global";
```

And mixins into any components that need them:

```
<style lang="scss" scoped>
@import "butter-mixins";
```

## Development

When you're doing development, create an MR for your branch, and maybe
re-run your CI publish job.  You'll be given a MR-specific version
that you can use to develop with before the MR gets merged
(e.g. 0.1.6-mr4.5).  The version number is, whatever the latest master
branch version, followed by `mr<ID>.<commits>`, where `<ID>` is the MR
ID, and `<commits>` is the number of commits since `master`.

## Icons
Add a `.vue` file with `<template>` surrounding the `<svg>` to `components/icons`.

_close.vue_
```
<template>
  <svg><!-- svg contents --></svg>
</template>
```
### Usage
```
<butter-icon name="close" />
```

## TODO:

We want to mimic the style guides as closely as possible, defining
things using cascading stylesheets where possible.

## PartnerMessage
`title, message, form, userActions, partner, limit, limitLabel`

### Form
`value, type, name, label, verify, placeholder, link`

`userActions` can include a require property that will disable the button until the required input is filled.
```
{ 
  title: 'Register',
  require: ['email','password'],
}
```
