import Vue from 'vue';

const requireComponent = require.context('butter-vue/components',true,/^[^_].*?\.(vue|js)$/)

requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName);
  const componentName = fileName.replace(/^\.\//, '').replace(/\.\w+$/, '').replace(/ui\//g,'').replace(/\//g,'-');

  Vue.component(componentName,componentConfig.default || componentConfig);
});

require('butter-vue/lib/butter-components.js')
