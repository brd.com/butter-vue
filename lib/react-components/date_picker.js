'use strict';
import React from 'react'
const e = React.createElement;

const MONTH_NAMES = ['January','Febuary','March','April','May','June','July','August','September','October','November','December']
const WEEK_DAYS = ['Mo','Tu','We','Th','Fr','Sa','Su']
const ONEDAY = 24 * 60 * 60 * 1000

class DatePicker extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedRange: 0,
      dateRanges: [{
        label: 'Now',
        value: Date.now()
      }, {
        label: 'Last day',
        value: Date.now() - ONEDAY
      }],
    }
    this.showLogs = this.showLogs.bind(this);
  }

  selectRange(index) {
    this.setState({
      selectedRange: index
    })
  }

  calendars() {
    const lastMonth = this.previousMonth()
    return [{
      header: this.calendarHeader(lastMonth),
      date: lastMonth,
      previousMonthWeekDays: this.leftoverDays(lastMonth),
      previousMonthDays: this.daysInMonth(this.previousMonth(lastMonth))
    }, {
      header: this.calendarHeader(),
      date: new Date(),
      previousMonthWeekDays: this.leftoverDays(),
      previousMonthDays: this.daysInMonth(this.previousMonth())
    }]
  }

  makeDate({day=((new Date()).getDay()), month=((new Date()).getMonth()+1), year=((new Date()).getFullYear())}) {
    return new Date(`${year}-${month}-${day}`)
  }

  daysInMonth(date = new Date()) {
    const start = new Date(`${date.getFullYear()}-${date.getMonth()+1}-01`)
    const end = new Date(`${date.getFullYear()}-${date.getMonth()+2}-01`)
    const days = (end.getTime() - start.getTime()) / ONEDAY
    return Math.ceil(days)
  }

  leftoverDays(date = new Date()) {
    const firstOfMonth = new Date(`${date.getFullYear()}-${date.getMonth()+1}-01`)
    return firstOfMonth.getDay()
  }

  calendarHeader(date = new Date()) {
    return `${MONTH_NAMES[date.getMonth()]} ${date.getFullYear()}`
  }

  previousMonth(date = new Date()) {
    return this.makeDate({ month: date.getMonth(), day: '1' })
  }

  buildCalendar(days, label) {
    const calendar = []
    for (let i = 1; i < days; i++) {
      calendar.push(label(i))
    }
    return calendar
  }

  dateCompare(a, b) {
    if (typeof a === 'undefined' || typeof b === 'undefined') return false
    if (a.getMonth() !== b.getMonth()) return false
    if (a.getDate() !== b.getDate()) return false
    return true
  }

  showLogs() {
    const range = this.state.dateRanges[this.state.selectedRange]
    const selectedDate = new Date(range.value)
    const event = new CustomEvent('DatePicker:showLogs', { detail: selectedDate.getTime() })
    document.dispatchEvent(event)
  }

  render() {
    return e(
      'div',
      { className: 'date-picker' },
      e(
        'div',
        { className: 'header' },
        e('h1', {}, 'Date Range')
      ),
      e('section', { className: 'main' },
        e(
          'div',
          { className: 'select-list' },
          this.state.dateRanges.map((range, index) => {
            return e(
              'button',
              {
                className: (this.state.selectedRange === index ? 'selected' : ''),
                key: index,
                onClick: this.selectRange.bind(this, index)
              },
              range.label
            )
          })
        ),
        e(
          'div',
          { className: 'calendar' },
          this.calendars().map((calendar, index) => {
            return e(
              'div',
              { className: 'calendar-month', key: index },
              e('h1', null, calendar.header),
              e(
                'div',
                { className: 'calendar-week-header' },
                WEEK_DAYS.map((day, index) => {
                  return e('span', { key: index }, day)
                })
              ),
              e(
                'div',
                { className: 'calendar-week-days' },
                this.buildCalendar(
                  calendar.previousMonthWeekDays,
                  (i) => {
                    return e(
                      'span',
                      { key: i },
                      calendar.previousMonthDays - calendar.previousMonthWeekDays + i
                    )
                  }
                ),
                this.buildCalendar(
                  this.daysInMonth(calendar.date),
                  (i) => {
                    const range = this.state.dateRanges[this.state.selectedRange]
                    const selectedDate = new Date(range.value)
                    const className = this.dateCompare(this.makeDate({ day: i, month: calendar.date.getMonth()+1 }), selectedDate) ? 'current selected' : 'current'
                    return e('span', { key: i, className }, i)
                  }
                )
              )
            )
          })
        )
      ),
      e(
        'div',
        { className: 'actions' },
        e(
          'div',
          { className: 'flex row' },
          e(
            'button',
            {
              className: 'large',
              onClick() {
                const event = new CustomEvent('DatePicker:closeModal')
                document.dispatchEvent(event)
              }
            },
            'Cancel'
          ),
          e(
            'button',
            {
              className: 'primary large',
              onClick: this.showLogs
            },
            'Show Logs'
          )
        )
      )
    );
  }
}

export default DatePicker
