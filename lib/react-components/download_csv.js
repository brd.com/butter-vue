'use strict';
import React from 'react'
const e = React.createElement

import DownArrow from 'butter-vue/lib/react-components/down-arrow.js'

class DownloadCSV extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      logs: props.logs
    }
    this.downloadCSV = this.downloadCSV.bind(this)
  }

  downloadCSV() {
    window.open(encodeURI("data:text/csv;charset=utf-8,"
    + this.state.logs.map(e => e.map(_ => _.value).join(",")).join("\n")))
  }

  render() {
    return e(
      'button',
      {
        className: 'filter ui',
        onClick: this.downloadCSV
      },
      DownArrow,
      'Download Logs'
    )
  }
}

export default DownloadCSV
