import Vue from 'vue'
Vue.component('description-list', {
  props: ['term', 'description','link'],
  render: function(createElement) {
    return createElement(this.link ? 'a' : 'div', {
      class: 'description-list',
      attrs: { href: this.link },
    }, [
      this.$slots.default,
      createElement('div', {
        class: 'content'
      }, [
        createElement('h2', this.term),
        createElement('span', this.description),
      ]),
      this.link && createElement('span', {
        class: 'arrow-right',
      })
    ]);
  },
})
Vue.component('transaction', {
  render: function (createElement) {
    return createElement('div', {
      class: `transaction-item`
    }, [
      createElement('div', [
        createElement('div', { class: 'icon' }, this.$slots.default),
        createElement('flex', {
          attrs: { dir: 'column' }
        }, [
          createElement('span', this.label),
          createElement('span', { class: 'sub' }, this.completed)
        ])
      ]),
      createElement('div', [
        createElement('span', { class: 'receive' }, this.received),
        createElement('span', { class: 'sub' }, `-${this.sent}`),
      ])
    ]);
  },
  props: ['label', 'received', 'sent','completed'],
})
Vue.component('flex', {
  render: function (createElement) {
    return createElement('div', {
      class: `flex ${this.dir}`
    }, this.$slots.default);
  },
  props: {
    'dir': {
      default: 'row',
      type: String,
    }
  },
})
Vue.component('progress-icon', {
  props: ['value'],
  render: function(createElement) {
    return createElement('div',
    {
      class: {
        'progress-icon': true,
        'icon': true,
        blank:this.value==0,
        partial:0<this.value&&this.value<1,
        done:1<=this.value
      }
    },
    [
      createElement('svg', {
        class: "ring",
        attrs: {
          'xmlns': "http://www.w3.org/2000/svg",
          'viewBox': "0 0 36 36"
        }
      }, [
        createElement('path', {
          attrs: {
            d: "M18 2.0845 a 15.9155 15.9155 0 0 1 0 31.831 a 15.9155 15.9155 0 0 1 0 -31.831",
            'stroke-dasharray': `${(Math.min(1,this.value)*100).toFixed(0)}, 100`
          }
        })
      ]),
      createElement('div', {
        class: "inner"
      }, this.$slots.default)
    ])
  },
})
Vue.component('named-list', {
  props: ['label'],
  render: function(createElement) {
    return createElement('flex', {
      props: {
        dir: 'row',
      },
      class: 'named-list',
    }, [
      createElement('div', this.label),
      createElement('div', this.$slots.default),
    ])
  },
})
Vue.component('modal', {
  props: {
    'title': {
      required: false,
    },
    'headerClass': {
      required: false,
    },
    'size': {
      default: 'page'
    }
  },
  render: function(createElement) {
    const that = this;
    const modal = createElement('div', {
      class: ['modal', this.size],
      style: {
        overflow: 'auto',
      },
    }, [
      createElement('div', {
        class: ['header', this.headerClass],
      }, [
        createElement('div', {
          class: 'close',
          on: {
            'click': function() { that.$emit('close') },
          },
        }, this.$slots.close),
        this.$slots.header,
        createElement('h1', this.title)
      ]),
      createElement('div', {
        class: 'content'
      }, this.$slots.default),
      createElement('div', {
        class: 'actions'
      }, this.$slots.actions),
    ]);
    if (this.size != 'page') return createElement('div', { class: 'modal-overlay' }, [modal]);
    return modal;
  },
})
