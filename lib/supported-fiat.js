export const FIAT = [
  { currency: 'USD', precision: 2, symbol: '$', name: 'US Dollar' },
  { currency: 'EUR', precision: 2, symbol: '€', name: 'Eurozone Euro' },
  { currency: 'GBP', precision: 2, symbol: '£', name: 'Pound Sterling' },
  { currency: 'DKK', precision: 2, name: 'Danish Krone' },
  { currency: 'CAD', precision: 2, symbol: '$', name: 'Canadian Dollar' },
  { currency: 'JPY', precision: 2, symbol: '¥', name: 'Japanese Yen' },
  { currency: 'RUB', precision: 2, name: 'Russian Ruble' },
  { currency: 'AUD', precision: 2, symbol: '$', name: 'Australian Dollar' },
  { currency: 'KRW', precision: 2, symbol: '₩', name: 'South Korean Won' },
  { currency: 'CHF', precision: 2, symbol: '₣', name: 'Swiss Franc' },
  { currency: 'SEK', precision: 2, name: 'Swedish Krona' },
  { currency: 'ILS', precision: 2, symbol: '₪', name: 'Israeli Shekel' },
  { currency: 'AED', precision: 2, symbol: 'د.إ', name: 'UAE Dirham' },
  { currency: 'ARS', precision: 2, symbol: '$', name: 'Argentine Peso' },
  { currency: 'AZN', precision: 2, symbol: '₼', name: 'Azerbaijani Manat' },
  { currency: 'BGN', precision: 2, symbol: 'лв.', name: 'Bulgarian Lev' },
  { currency: 'BRL', precision: 2, symbol: 'R$', name: 'Brazilian Real' },
  { currency: 'CLP', precision: 2, symbol: '$', name: 'Chilean Peso' },
  { currency: 'COP', precision: 2, symbol: '$', name: 'Colombian Peso' },
  { currency: 'CRC', precision: 2, symbol: '₡', name: 'Costa Rican ColÃ³n' },
  { currency: 'CZK', precision: 2, symbol: 'Kč', name: 'Czech Koruna' },
  { currency: 'DOP', precision: 2, symbol: 'RD$', name: 'Dominican Peso' },
  { currency: 'GEL', precision: 2, symbol: '₾', name: 'Georgian Lari' },
  { currency: 'HKD', precision: 2, symbol: 'HK$', name: 'Hong Kong Dollar' },
  { currency: 'HUF', precision: 2, symbol: 'Ft', name: 'Hungarian Forint' },
  { currency: 'INR', precision: 2, symbol: '₹', name: 'Indian Rupee' },
  { currency: 'KZT', precision: 2, symbol: '₸', name: 'Kazakhstani Tenge' },
  { currency: 'MAD', precision: 2, symbol: 'DH', name: 'Moroccan Dirham' },
  { currency: 'MDL', precision: 2, symbol: 'L', name: 'Moldovan Leu' },
  { currency: 'MXN', precision: 2, symbol: 'Mex$', name: 'Mexican Peso' },
  { currency: 'MYR', precision: 2, symbol: 'RM', name: 'Malaysian Ringgit' },
  { currency: 'NAD', precision: 2, symbol: 'N$', name: 'Namibian Dollar' },
  { currency: 'NGN', precision: 2, symbol: '₦', name: 'Nigerian Naira' },
  { currency: 'NOK', precision: 2, symbol: 'kr', name: 'Norwegian Krone' },
  { currency: 'NZD', precision: 2, symbol: 'NZ$', name: 'New Zealand Dollar' },
  { currency: 'PEN', precision: 2, symbol: 'S/', name: 'Peruvian Nuevo Sol' },
  { currency: 'PHP', precision: 2, symbol: '₱', name: 'Philippine Peso' },
  { currency: 'PLN', precision: 2, symbol: 'zł', name: 'Polish Zloty' },
  { currency: 'QAR', precision: 2, symbol: 'QR', name: 'Qatari Rial' },
  { currency: 'RON', precision: 2, symbol: 'L', name: 'Romanian Leu' },
  { currency: 'SGD', precision: 2, symbol: 'S$', name: 'Singapore Dollar' },
  { currency: 'TRY', precision: 2, symbol: '₺', name: 'Turkish Lira' },
  { currency: 'TWD', precision: 2, symbol: 'NT$', name: 'New Taiwan Dollar' },
  { currency: 'UAH', precision: 2, symbol: '₴', name: 'Ukrainian Hryvnia' },
  { currency: 'UYU', precision: 2, symbol: '$', name: 'Uruguayan Peso' },
  { currency: 'UZS', precision: 2, symbol: "so'm", name: 'Uzbekistani Som' },
  { currency: 'VND', precision: 2, symbol: '₫', name: 'Vietnamese Dong' },
  { currency: 'ZAR', precision: 2, symbol: 'R', name: 'South African Rand' },
];
