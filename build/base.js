module.exports = {
  mode: 'none',  
  resolve: {
    extensions: [
      ".mjs",
      ".js",
      ".json",
      ".vue",
      ".jsx",
      ".ts",
      ".tsx"
    ],
    alias: {
      '~': '/application',
      '~~': '/application',
      'assets': '/application/assets',
    },
  },
  module: {
    rules: [
      // ... other rules
      {
        test: /\.vue$/,
        loader: 'vue-loader'
      },
      {
        test: /\.scss$/,
        use: [
          'vue-style-loader',
          'css-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.jsx?$/i,
        use: [
          { loader: 'babel-loader',
            options: {
              presets: ['@nuxt/babel-preset-app']
            }
          }
        ],
      },
      {
        test: /\.(png|jpe?g|gif|svg|webp)$/i,
        use: [{
          loader: 'url-loader',
          options: { name: 'img/[hash:7].[ext]' }
        }]
      },
      {
        test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/i,
        use: [{
          loader: 'url-loader',
          options: { name: 'fonts/[hash:7].[ext]' },
        }]
      },
      {
        test: /\.(webm|mp4|ogv)$/i,
        use: [{
          loader: 'file-loader',
          options: { name: 'videos/[hash:7].[ext]' }
        }]
      }
    ]
  },
  plugins: [
    // make sure to include the plugin!
    new VueLoaderPlugin()
  ],
  optimization: {
    minimize: false
  },
};
