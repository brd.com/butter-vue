### Build Directory

These are not currently being used, they were experimental in case we want to
somehow pre-build butter-vue for other projects.  The current mechanism works by
adding butter-vue to the list of "externals" to process by webpack.  Once we get
closer to ready, we can re-instate this mechanism.
