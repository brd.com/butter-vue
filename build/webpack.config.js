const VueLoaderPlugin = require('vue-loader/lib/plugin')

const util = require('util');
const fs = require('fs');
const path = require('path');

const componentsDir = path.resolve(__dirname,'../components');
const componentsList = fs.readdirSync(componentsDir);
const components = {};

for(i in componentsList) {
  var f = componentsList[i];

  var n = f.replace(/([a-z])([A-Z])/g,(m,a,b) => `${a}-${b.toLowerCase()}`)
      .replace(/^([A-Z])/g,(m,a) => a.toLowerCase())
      .replace(/.vue$/g,'');
  
  var p = path.join(componentsDir,f);

  components[n] = p;
}

const base = require('./base.js');

module.exports = [
  {
    ...base,
    entry: components,
    output: {
      // publicPath: '/dist/',
      filename: 'components/[name].js',
      chunkFilename: '[id].js',
      libraryTarget: 'commonjs2'
    },
  },
  {
    ...base,
    entry: {
      index: path.resolve(__dirname,'../index.js'),
    },
    output: {
      filename: '[name].js',
      libraryTarget: 'commonjs2',
    },
  }
];
