const fs = require('fs');
const path = require('path');

module.exports = {
  head: {
    title: 'Butter-Vue',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport',
        content: 'width=device-width, initial-scale=1.0, minimum-scale=1.0, user-scalable=no, viewport-fit=cover', },
      { hid: 'description', name: 'description', content: 'Butter Component Library' },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],
  },
  modules: [
    path.resolve(__dirname,'module.js'),
  ],
  build: {
    extend(config,{isClient,isServer}) {

    }
  },
  components: {
    dirs: [
      '~/components',
      '~/components/icons',
      '~/components/layout',
      '~/components/blockset',
    ],
  },
};
