const path = require('path')

function loader(rules,name,callback) {
  for(i in rules) {
    const rule = rules[i]
    if(rule.loader == name) callback(rule);
    if(rule.use) loader(rule.use,name,callback);
    if(rule.oneOf) loader(rule.oneOf,name,callback);
  }
}

module.exports = function (moduleOptions) {
  this.nuxt.hook('build:before',(nuxt,buildOptions) => {
    buildOptions.build = buildOptions.build || {};
    buildOptions.build.transpile = buildOptions.build.transpile || ['butter-vue'];

    var extend = buildOptions.extend;

    buildOptions.extend = function(config,ctx) {
      extend.call(this,config,ctx);

      loader(config.module.rules,'sass-loader',(rule) => {
        const options = rule.options = rule.options || {};
        options.sassOptions = options.sassOptions || {};
        options.sassOptions.includePaths = options.sassOptions.includePaths || [];
        const pathToInclude = path.resolve(__dirname,'assets/styles');

        if(!options.sassOptions.includePaths.includes(pathToInclude)) {
          options.sassOptions.includePaths.push(pathToInclude);
        }
      });

      loader(config.module.rules,'file-loader',(rule) => {
      });

      const resolve = config.resolve || {};
      const alias = resolve.alias || {};

      alias['butter-vue'] = path.resolve(__dirname); //,'./assets/fonts');
    };
  });

  this.addPlugin(path.resolve(__dirname,'./plugins/components.js'));
};
